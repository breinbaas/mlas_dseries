from pydantic import BaseModel
from pathlib import Path

from mlas.models.calculationmodel import CalculationModel
from mlas.helpers import case_insensitive_glob

from geolib.models.dstability.dstability_model import DStabilityModel
from geolib.models.dstability.analysis import (
    DStabilitySearchGrid, 
    DStabilitySearchArea, 
    DStabilitySlipPlaneConstraints, 
    DStabilityBishopBruteForceAnalysisMethod
)
from geolib.soils import Soil, MohrCoulombParameters, SoilWeightParameters, ShearStrengthModelTypePhreaticLevel
from geolib.soils.soil_utils import Color
from geolib.models.dstability.loads import UniformLoad
from geolib.geometry import Point

class DStability(BaseModel):
    calculationmodel: CalculationModel

    def to_file(self, filepath: str, filename: str):
        filename = f"{filename}.stix"
        dstability_model = DStabilityModel(filename=filename)

        for soil in self.calculationmodel.soils:
            mohr_coulomb_parameters = MohrCoulombParameters(
                cohesion=soil.stability_parameters.cohesion,
                dilatancy_angle=soil.stability_parameters.dilatancy_angle,
                friction_angle=soil.stability_parameters.friction_angle
            )

            soil_weight_parameters = SoilWeightParameters(
                saturated_weight = soil.y_dry,
                unsaturated_weight = soil.y_sat
            )

            if not dstability_model.soils.has_soilcode(soil.code):
                soil = Soil(
                    name=soil.code, 
                    code=soil.code, 
                    color=Color(soil.color),
                    mohr_coulomb_parameters=mohr_coulomb_parameters,
                    soil_weight_parameters=soil_weight_parameters,
                    shear_strength_model_above_phreatic_level=ShearStrengthModelTypePhreaticLevel.C_PHI,
                    shear_strength_model_below_phreatic_level=ShearStrengthModelTypePhreaticLevel.C_PHI
                )
                _ = dstability_model.add_soil(soil)
            else:
                # todo > rare manier van editen als je niet dezelfde namen kunt gebruiken.. geolib issue
                dstability_model.edit_soil(
                    code=soil.code, 
                    cohesion=soil.stability_parameters.cohesion,
                    dilatancy=soil.stability_parameters.dilatancy_angle,
                    friction_angle=soil.stability_parameters.friction_angle,
                    volumetric_weight_above_phreatic_level=soil.y_dry, 
                    volumetric_weight_below_phreatic_level=soil.y_sat 
                ) 

        for soilpolygon in self.calculationmodel.soilpolygons:
            points = [Point(x=p.x, z=p.z) for p in soilpolygon.points]
            dstability_model.add_layer(points=points, soil_code=soilpolygon.soilcode)

        # add phreatic line
        if len(self.calculationmodel.phreaticline) > 0:
            pl_id = dstability_model.add_head_line(
                points = [Point(x=p.x, z=p.z) for p in self.calculationmodel.phreaticline],
                label = "Phreatic Level",
                notes = "MLAS auto generated phreatic line",
                is_phreatic_line = True    
            )

            if len(self.calculationmodel.acquiferline) > 0:
                aq_id = dstability_model.add_head_line(
                    points = [Point(x=p.x, z=p.z) for p in self.calculationmodel.acquiferline],
                    label = "Acquifer Level",
                    notes = "MLAS auto generated acquifer line"
                )            

                # TODO reference lines maken
                # deze lijn moet de bovenzijde van de cohesieve lagen voorstellen
                # dstability_model.add_reference_line(
                #     points=[Point(x=-50, z=-3), Point(x=50, z=-3)],
                #     bottom_headline_id=pl_id,
                #     top_head_line_id=pl_id,
                # )

                # en deze lijn moet de bovenzijde van het watervoerend pakket voorstellen
                # dstability_model.add_reference_line(
                #     points=[Point(x=-50, z=-10), Point(x=50, z=-10)],
                #     bottom_headline_id=aq_id,
                #     top_head_line_id=aq_id,
                # )

        # add calculation settings for DSeries
        if self.calculationmodel.dstability_bishopbruteforce_settings:
            dstability_model.set_model(
                DStabilityBishopBruteForceAnalysisMethod(
                    search_grid=DStabilitySearchGrid(
                        bottom_left=Point(x=self.calculationmodel.dstability_bishopbruteforce_settings.x_min, z=self.calculationmodel.dstability_bishopbruteforce_settings.z_min),
                        number_of_points_in_x=self.calculationmodel.dstability_bishopbruteforce_settings.num_x,
                        number_of_points_in_z=self.calculationmodel.dstability_bishopbruteforce_settings.num_z,
                        space=self.calculationmodel.dstability_bishopbruteforce_settings.spacing
                    ),
                    bottom_tangent_line_z=self.calculationmodel.dstability_bishopbruteforce_settings.tangent_z_min,
                    number_of_tangent_lines=self.calculationmodel.dstability_bishopbruteforce_settings.tangent_num,
                    space_tangent_lines=self.calculationmodel.dstability_bishopbruteforce_settings.tangent_spacing,
                )
            )

        if self.calculationmodel.dstability_traffic_load:
            load = UniformLoad(
                start = self.calculationmodel.dstability_traffic_load.x_left,
                end = self.calculationmodel.dstability_traffic_load.x_left + self.calculationmodel.dstability_traffic_load.width,
                magnitude = self.calculationmodel.dstability_traffic_load.magnitude,
                angle_of_distribution = self.calculationmodel.dstability_traffic_load.distribution_angle
            )
            dstability_model.add_load(load)

        dstability_model.serialize(Path(filepath) / filename)

if __name__=="__main__":   
    files = case_insensitive_glob(Path.cwd() / "testdata", '.json')
    for cfile in files:
        calculationmodel = CalculationModel.parse(cfile)
        dstab = DStability(calculationmodel=calculationmodel)
        dstab.to_file(Path.cwd() / "testdata", Path(cfile).stem)





